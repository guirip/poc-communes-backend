
# README #

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Set up database

- You must install mongodb server
- Then run: `npm run import-communes`

Now you have a database containing all the french city names. You can check it with commands:

```bash
$ mongo
```

```mongoshell
$ use poc-communes
$ db.communes.count()
```

The output should be a number around 35712.


## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```