import { Module } from '@nestjs/common';
import { AppController } from './app.controller.js';
import { AppService } from './app.service.js';
import { CommunesModule } from './communes/communes.module.js';

@Module({
  imports: [CommunesModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
