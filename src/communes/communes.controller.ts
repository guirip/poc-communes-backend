import { Controller, Get, Param } from '@nestjs/common';
import { CommunesService } from './communes.service.js';

@Controller('communes')
export class CommunesController {
  constructor(private communesService: CommunesService) {}

  @Get(':nameStartsWith')
  async find(@Param() params) {
    const communes = await this.communesService.find(params.nameStartsWith);

    // split metropole / DOM-TOM
    const metropole = [];
    const domToms = [];

    for (const commune of communes) {
      if (commune.departement > 95) {
        domToms.push(commune);
      } else {
        metropole.push(commune);
      }
    }

    return {
      metropole,
      domToms,
    };
  }
}
