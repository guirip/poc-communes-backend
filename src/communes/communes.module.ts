import { Module } from '@nestjs/common';
import { CommunesController } from './communes.controller.js';
import { CommunesService } from './communes.service.js';

@Module({
  controllers: [CommunesController],
  providers: [CommunesService],
  exports: [CommunesService],
})
export class CommunesModule {}
