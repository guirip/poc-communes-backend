import { Injectable } from '@nestjs/common';
import CommuneModel from '../db/models/Commune.js';
import { Commune } from './interfaces/commune.interface';
import config from '../config.js';

@Injectable()
export class CommunesService {

  async find(nameStartsWith: string): Promise<Commune[]> {
    const criteria = {
      // TODO: it would be better to escape some characters (like '.' '*' '\' etc)
      [config.communes.searchOn]: new RegExp(`^${nameStartsWith}.*$`),
    };
    return await CommuneModel.find(criteria, null, { lean: true })
      .sort({
        [config.communes.sort.field]: config.communes.sort.order,
      })
      .limit(config.communes.maxResults);
  }
}
