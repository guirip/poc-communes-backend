export interface Commune {
  departement: number,
  codePostal: string;
  libelleAcheminement: string;
  codeCommune: string;
  nomCommune: string;
}
