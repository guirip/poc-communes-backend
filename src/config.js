export default {
  communes: {
    maxResults: 100,
    searchOn: 'nomCommune',
    sort: {
      field: 'nomCommune',
      order: 1, // 1: ascending order, -1: descending order
    },
  },
  db: {
    host: '127.0.0.1',
    port: 27017,
    instance: 'poc-communes',
  },
};
