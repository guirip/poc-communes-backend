import mongoose from 'mongoose';

import config from '../config.js';

/**
 * Open the default connection to mongodb
 */
export async function connect() {
  await mongoose.connect(
    `mongodb://${config.db.host}:${config.db.port}/${config.db.instance}`,
    {
      keepAlive: true,
    },
  );
}

/**
 * Close the default connection to mongodb
 * @public by pattern
 */
export const disconnect = () => new Promise((resolve) => {
  mongoose.disconnect((err) => {
    resolve(err);
  });
});

// EVENT HANDLERS

/**
 * Handle mongoose connection error event
 * @param err
 * @private
 */
function onError(err) {
  console.error('Mongoose error: ' + err);
}
mongoose.connection.on('error', onError);

/**
 * Handle mongoose connection close event
 * @private
 */
function connectionClosed() {
  console.log('Mongoose connection close');
}
mongoose.connection.on('disconnected', connectionClosed);
