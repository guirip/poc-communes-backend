import got from 'got';
import { connect, disconnect } from './DatabaseService.js';
import Commune from './models/Commune.js';

const URL =
  'https://www.data.gouv.fr/fr/datasets/r/34d4364c-22eb-4ac0-b179-7a1845ac033a';

const BATCH_SIZE = 500;

const DEPARTEMENT_REGEXP = /^(\d{1,2})\d{3}$/;

/**
 * Add departement number to commune
 * @param  {object} commune
 * @return  {object} updated commune
 */
function addDepartement(commune) {
  if (!commune.codePostal || commune.codePostal.length < 4) {
    console.warn('This commune has invalid `codePostal` data: ', JSON.stringify(commune));
  } else {
    const [, deptAsString] = DEPARTEMENT_REGEXP.exec(commune.codePostal);
    commune.departement = parseInt(deptAsString, 10);
  }
  return commune;
}

/**
 * Return the next batch of communes to insert into database
 * @param {array} data
 * @return {array}
 */
const getBatch = data => data.splice(0, BATCH_SIZE).map(addDepartement);

/**
 * Download and import 'commune' data to database
 */
async function importer() {
  let data;
  try {
    const { body } = await got(URL, { responseType: 'json' });
    data = body;
  } catch (e) {
    console.error('Failed to download commune data', e);
  }

  console.info(`About to import ${data.length} 'communes' to database ⏳`);

  // Open DB connection
  await connect();

  // Clear existing data
  await Commune.deleteMany({});

  // Insert data
  let imported = 0;
  let batch = getBatch(data);
  while (batch.length > 0) {
    await Commune.create(batch);

    imported += batch.length;
    console.log(`${imported} imported`);

    batch = getBatch(data);
  }

  // Close DB connection
  await disconnect();
  console.info('Done ✅');
}

(async () => {
  await importer();
})();
