import mongoose from 'mongoose';

const schema = new mongoose.Schema({
  departement: Number,
  codePostal: String, // code postal
  libelleAcheminement: String, // libellé d'acheminement (en majuscules non accentuées à la norme postale)
  codeCommune: String, // code INSEE de la commune
  nomCommune: String,
});

export default mongoose.model('Commune', schema);
