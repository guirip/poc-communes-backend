import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module.js';
import { connect, disconnect } from './db/DatabaseService.js';

let alreadyShuttingdown = false;

async function bootstrap() {
  // Open DB connection
  await connect();

  const app = await NestFactory.create(AppModule);
  app.enableCors();
  await app.listen(3000);
}
bootstrap();

// catch ctrl+c event
process.on('SIGINT', stop);

// catch uncaught exceptions
process.on('uncaughtException', stop);

/**
 * Shutdown server
 * @param {object} e
 * @param {number} code
 */
async function stop(e, code) {
  if (alreadyShuttingdown) return;

  alreadyShuttingdown = true;
  if (e && e.stack) {
    console.error(e.stack);
  }
  // Close DB connection
  await disconnect();

  process.exit(
    code
      ? code
      : e ? 1 : 0
  );
}
