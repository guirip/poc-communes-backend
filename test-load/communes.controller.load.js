
import autocannon from 'autocannon';
import { connect, disconnect } from '../src/db/DatabaseService.js';
import Commune from '../src/db/models/Commune.js';


const getRandomInt = (min=0, max=512) => (
  Math.floor(Math.random() * Math.abs(max-min)) + min
);

const getRandomFirstChars = string => (
  string.substring(0, getRandomInt(1, 5))
);

(async function() {

  const sampleAmount = 300;
  const serverUrl = `http://localhost:3000`;
  const controller = '/communes'
  const method = 'GET';
  let paths;

  await connect();
  try {
    const communes = await Commune.find({}, 'nomCommune', { lean: true }).limit(sampleAmount);

    paths = communes.map(({ nomCommune }) => `${controller}/${getRandomFirstChars(nomCommune)}`);

    await disconnect();

  } catch(e) {
    console.error('Une erreur est survenue', e);
    await disconnect();
    throw e;
  }

  const getRandomRequest = request => {
    request.method = method;
    request.path = paths[getRandomInt(0, paths.length)];
    return request;
  };

  console.info('Starting test on ${controller}/{string} HTTP '+method);

  const instance = autocannon({
    url: serverUrl,
    connections: 100,
    duration: 15,
    headers: {
      'Content-Type': 'application/json',
    },
    requests: [{ setupRequest: getRandomRequest }],
  });

  autocannon.track(instance);
})();
